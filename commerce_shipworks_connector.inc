<?php

define('PLATFORM' , 'Drupal Commerce');
define('DEVELOPER' , 'Name Developer');
define('STORE_NAME' , 'Name of store');
define('STORE_OWNER' , 'Name of store owner');
define('STORE_OWNER_EMAIL_ADDRESS' , 'Email address of store owner');
define('STORE_STATE' , 'State');
define('STORE_COUNTRY' , 'Country');
define('STORE_WEBSITE' , 'http://www.example.com');
define('STORE_TIME_ZONE', 'UTC');
define('STORE_CURRENCY', 'USD');
define('REQUIRE_SECURE', true);
define('SHIPWORKS_TRACE', TRUE);

function commerce_shipworks_connector_endpoint() {
	//watchdog('shipworks', 'Endpoint Entered');

	try {
	  $moduleVersion = "3.0.0";
	  $schemaVersion = "1.0.0";
	
	  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	
	  // HTTP/1.1
	  header("Content-Type: text/xml;charset=utf-8");
	  header("Cache-Control: no-store, no-cache, must-revalidate");
	  header("Cache-Control: post-check=0, pre-check=0", false);
	
	  // HTTP/1.0
	  header("Pragma: no-cache");
	
	  $secure = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == '1'));
	
	  // Open the XML output and root
	  _writeXmlDeclaration();
	  _writeStartTag("ShipWorks", array("moduleVersion" => $moduleVersion, "schemaVersion" => $schemaVersion));
  } catch (Exception $e) {
		_RecordException($e);
	}
  // Enforse SSL
  if (!$secure && REQUIRE_SECURE) {
    _outputError(10, 'A secure (https://) connection is required.');
  }

  else {
  	try {
	    $action = (isset($_REQUEST['action']) ? $_REQUEST['action'] : '');
	    if ( SHIPWORKS_TRACE ) {
	    	if ( !empty($_REQUEST['password']) ) {
	    		unset($_REQUEST['password']);
	    	}
	    	
	    	watchdog('shipworks', "Action: $action<pre>" . print_r($_REQUEST, TRUE) . '</pre>');
	    }
	    
	    switch (strtolower($action)) {
	      case 'getmodule': _Action_GetModule(); break;
	      case 'getstore': _Action_GetStore(); break;
	      case 'getcount': _Action_GetCount(); break;
	      case 'getorders': _Action_GetOrders(); break;
	      case 'getstatuscodes': _Action_GetStatusCodes(); break;
	      case 'updatestatus': _Action_UpdateStatus(); break;
	      case 'updateshipment': _Action_UpdateShipment(); break;
	      default: _outputError(70, "'$action' not supported");
	    }
	  } catch (Exception $e) {
			_RecordException($e);
		}
  }

    // Close the output
  _writeCloseTag("ShipWorks");

  // Exit so we don't do any extra/unnecessary rendering.
  drupal_exit();

}

function _RecordException($e) {
	watchdog('shipworks', 'Caught exception: "' . $e->getMessage() .  '" in ' . $e->file . ', ' . $e->line, WATCHDOG_CRITICAL);
}


// Get module data
function _Action_GetModule() {
  _writeStartTag("Module");

    _writeElement("Platform", PLATFORM);
    _writeElement("Developer", DEVELOPER);

    _writeStartTag("Capabilities");
      _writeElement("DownloadStrategy", "ByModifiedTime");
      _writeFullElement("OnlineCustomerID", "", array("supported" => "false"));
      _writeFullElement("OnlineStatus", "", array("supported" => "true", "dataType" => "text", "supportsComments" => "false" ));
      _writeFullElement("OnlineShipmentUpdate", "", array("supported" => "true"));
    _writeCloseTag("Capabilities");

   _writeCloseTag("Module");
}

// Write store data
function _Action_GetStore() {
  _writeStartTag("Store");
    _writeElement("Name", STORE_NAME);
    _writeElement("CompanyOrOwner", STORE_OWNER);
    _writeElement("Email", STORE_OWNER_EMAIL_ADDRESS);
    _writeElement("State", STORE_STATE);
    _writeElement("Country", STORE_COUNTRY);
    _writeElement("Website", STORE_WEBSITE);
  _writeCloseTag("Store");
}

function _Action_GetStatusCodes() {
  _writeStartTag("StatusCodes");

	$codes = commerce_shipworks_connector_status_codes(TRUE);

	foreach ( $codes as $code => $name ) {
    _writeStartTag("StatusCode");
      _writeElement("Code", $code);
      _writeElement("Name", $name);
    _writeCloseTag("StatusCode");
  }

  _writeCloseTag("StatusCodes");
}

// Get the count of orders greater than the start ID
function _Action_GetCount() {
  // Set default timezone
  date_default_timezone_set(STORE_TIME_ZONE);

  $start = '2015-07-01T00:00:00';

  if (isset($_REQUEST['start'])) {
    $start = $_REQUEST['start'];
  }

  // Write the params for easier diagnostics
  _writeStartTag("Parameters");
      _writeElement("Start", $start);
  _writeCloseTag("Parameters");
	/*
  $ordersQuery = db_select('commerce_order', 'o');
  $ordersQuery->join('field_data_field_payment_status', 'ps', 'o.order_id = ps.entity_id');
  $ordersQuery->fields('o', array('order_id'));
  $ordersQuery->where('o.changed > :changed', array(':changed' => strtotime($start)));
  $ordersQuery->condition('ps.field_payment_status_value', array('paid'), 'IN');
  $ordersQuery->condition(
                  db_or()
                    ->condition('o.status', array('processing'), 'IN')
                    ->condition('o.status', array('pending'), 'IN')
  );
  */
  $ordersQuery = db_select('commerce_order', 'o');
  $ordersQuery->fields('o', array('order_id'));
  $ordersQuery->condition('o.changed', strtotime($start), '>');
  $ordersQuery->condition('o.status', array('processing', 'pending'), 'IN');
  
  $count = $ordersQuery->countQuery()->execute()->fetchField();

  _writeElement("OrderCount", (int) $count);
}

// Get all orders greater than the given start id, limited by max count
function _Action_GetOrders() {
	//watchdog('shipworks', "Get Orders");
	
  // Set default timezone
  date_default_timezone_set(STORE_TIME_ZONE);

  $start = '2015-07-01T00:00:00';
  $maxcount = 1;

  if (isset($_REQUEST['start'])) {
    $start = $_REQUEST['start'];
  }

  // Write the params for easier diagnostics
  _writeStartTag("Parameters");
    _writeElement("StartGMT", $start);

  if (isset($_REQUEST['maxcount'])) {
    $maxcount = $_REQUEST['maxcount'];
  }

  // Only get orders through 2 seconds ago.
  $end = date("Y-m-d\TH:i:s", time() - 2);

    _writeElement("StartLocal", $start);
    _writeElement("End", $end);
    _writeElement("MaxCount", $maxcount);
  _writeCloseTag("Parameters");

  _writeStartTag("Orders");
  
	/*
  $ordersQuery = db_select('commerce_order', 'o');
  $ordersQuery->join('field_data_field_payment_status', 'ps', 'o.order_id = ps.entity_id');
  $ordersQuery->fields('o', array('order_id', 'changed'));
  $ordersQuery->where('o.changed > :changed', array(':changed' => strtotime($start)));
  $ordersQuery->condition('ps.field_payment_status_value', array('paid'), 'IN');
  $ordersQuery->condition(
                  db_or()
                    ->condition('o.status', array('processing'), 'IN')
                    ->condition('o.status', array('pending'), 'IN')
  );
  */
  $ordersQuery = db_select('commerce_order', 'o');
  $ordersQuery->fields('o', array('order_id'));
  $ordersQuery->condition('o.changed', strtotime($start), '>');
  $ordersQuery->condition('o.status', array('processing', 'pending'), 'IN');

  $result = $ordersQuery->execute();

  foreach ($result as $row) {
    // Load order
    $order_id = $row->order_id;
    //watchdog('shipworks', "Order ID: $order_id");
    /*
		try {
	    if ( function_exists('commerce_order_load') ) {
		    $order = commerce_order_load($order_id);
		  }
		} catch (Exception $e) {
		    watchdog('shipworks', 'Caught exception: ' . $e->getMessage());
		}
		*/
		
    //watchdog('shipworks', 'Order<pre>' . print_r($order, TRUE) . '</pre>');
		try {
			_BuildOrder($order_id);
		} catch (Exception $e) {
			_RecordException($e);
		}
    
    //$order_wrapper = entity_metadata_wrapper('commerce_order', $order_id);

    //_WriteOrder($order_wrapper);
  }
    _writeCloseTag("Orders");
}
function _BuildOrder($order_id) {
	$order = commerce_order_load($order_id);

	
	//watchdog('shipworks', "Order:<pre>" . print_r($order, TRUE) . '</pre>');
	
	$order_wrapper = entity_metadata_wrapper('commerce_order', $order);
	//watchdog('shipworks', "Order Poperties:<pre>" . print_r($order_wrapper->getPropertyInfo(), TRUE) . '</pre>'); 
	
	if ( !empty($order->commerce_customer_billing) ) {	
		$profile_id = $order_wrapper->commerce_customer_billing->profile_id->value();
		
		//watchdog('shipworks', "Billing Profile ID:<pre>" . print_r($profile_id, TRUE) . '</pre>');
		
		$billing = commerce_customer_profile_load($profile_id);
		
		//watchdog('shipworks', "Billing Profile:<pre>" . print_r($billing, TRUE) . '</pre>');
		
		$billing_wrapper =  entity_metadata_wrapper('commerce_customer_profile', $billing);

		//watchdog('shipworks', "Billing Locality:<pre>" . print_r($billing_wrapper->commerce_customer_address->locality->value(), TRUE) . '</pre>');
	}

	if ( !empty($order->commerce_customer_shipping) ) {
		$profile_id = $order_wrapper->commerce_customer_shipping->profile_id->value();
		
		//watchdog('shipworks', "Shipping Profile ID:<pre>" . print_r($profile_id, TRUE) . '</pre>');
		
		$shipping = commerce_customer_profile_load($profile_id);

		//watchdog('shipworks', "Shipping Profile:<pre>" . print_r($shipping, TRUE) . '</pre>');
		
		$shipping_wrapper =  entity_metadata_wrapper('commerce_customer_profile', $shipping);
	}
	else {
		$shipping_wrapper = $billing_wrapper;
	}
	
	$weight = commerce_physical_order_weight($order, 'lb');
	
	_WriteOrder($order_wrapper, $weight, $billing_wrapper, $shipping_wrapper);
}

function _WriteOrder($order_wrapper, $weight, $billing, $shipping) {
	//watchdog('shipworks', "Order<pre>" . print_r($order_wrapper, TRUE) . "</pre>");
	//watchdog('shipworks', "Order Properties<pre>" . print_r($order_wrapper->getPropertyInfo(), TRUE) . "</pre>");
    
  // Shipping Method
  $transit_time = 0;
  $packages = 0;
  $shipping_method = '';
  
  foreach ($order_wrapper->commerce_line_items as $index => $line_item_wrapper) {
  	//watchdog('shipworks', 'Line Item:<pre>' . print_r($line_item_wrapper->getPropertyInfo(), TRUE) . '</pre>');

    if ($line_item_wrapper->type->value() == 'shipping') {
	
      $shipping_text = $line_item_wrapper->line_item_label->value();

      $parts = explode(' ', $shipping_text);
      
      if ( count($parts) > 0 )  {
      	$shipping_method = $parts[0];
      }
      
      $price = $line_item_wrapper->commerce_unit_price->value();
      foreach ( $price['data']['components'] as $key => $component ) {
      	if ( $component['name'] == 'shipping' ) {
      		if ( !empty($component['price']) && !empty($component['price']['data']) ) {
      			if ( !empty($component['price']['data']['transit_time']) ) {
      				$transit_time = $component['price']['data']['transit_time'];
      			}
      			if ( !empty($component['price']['data']['packages']) ) {
      				$packages = $component['price']['data']['packages'];
      			}
      		}	
      	}
      }
    }
  }

	watchdog('shipworks', "T: $transit_time, P: $packages");

	watchdog('shipworks', 'Weight: <pre>' . print_r($weight, TRUE) . '</pre>');

	
  if (empty($shipping_text)) {
  	watchdog('shipworks', 'No Shipping Method');
    $shipping_text = 'None';
  }
	
  // Notes
  //$admin_notes = $order_wrapper->field_order_admin_notes->value();
  $notes = array();
  
  $notes[] = strip_tags($shipping->field_special_instructions->value()); // strip_tags($admin_notes['value']);
	if ( !empty($weight) || $transit_time || $packages ) {
		$order_details = '';
		if ( !empty($weight) ) {
			$order_details .= format_plural($weight['weight'], "Total weight: @count lb\n", "Total weight: @count lbs\n");
		}
		if ( $packages ) {
			$order_details .= "Estimated packages: $packages\n";
		}
		if ( $transit_time ) {
			$singular = "Estimated transit time: @count day\n";
			$plural = "Estimated transit time: @count days\n";
			$order_details .= format_plural($transit_time, $singular, $plural);
		}
		$notes[] = $order_details;
	}
	
	if ( $transit_time || $packages ) {
		// Should be fedex or UPS
		$service = $shipping_text;
		$shppping_text = '';
		
		if ( !empty($shipping_method) ) {
			$shipping_text = $shipping_method;
		}
		
		if ( !empty($packages) || !empty($weight) ) {
			$shipping_text .= ' (';
		}
		
		if ( $packages ) {
			$shipping_text .= format_plural($packages, "@count pc.", "@count pcs.");
		}
		
			
		if ( !empty($weight) ) {
			$shipping_text .= format_plural($weight['weight'], "@countlb.", "@count lbs.");
		}
		
		if ( !empty($packages) || !empty($weight) ) {
			$shipping_text .= ')';
		}
		
		$shipping_text .= '(' . $service;
		if ( $transit_time ) {
			$shipping_text .= ' ' . format_plural($transit_time, "@count day", "@count days");
		}
		
		$shipping_text .= ')';
	}
	
  _writeStartTag("Order");

    _writeElement("OrderNumber", $order_wrapper->order_id->value());
    _writeElement("OrderDate", date("Y-m-d\TH:i:s", $order_wrapper->created->value()));
    _writeElement("LastModified", date("Y-m-d\TH:i:s", $order_wrapper->changed->value()));
    _writeElement("ShippingMethod", $shipping_text);
    _writeElement("StatusCode", $order_wrapper->status->value());

		if ( !empty($notes) ) {
	    _writeStartTag("Notes");
	    	foreach ( $notes as $note ) {
		      _WriteNote($note, date("Y-m-d\TH:i:s", $order_wrapper->changed->value()));
		    }
	    _writeCloseTag("Notes");
	  }

		try {
	   	//watchdog('shipworks', "Shipping Address Properties<pre>" . print_r($shipping->getPropertyInfo(), TRUE) . '</pre>');
	  	$shipping_address = $shipping->commerce_customer_address;
	  	$phone = $shipping->field_telephone_number->value();
	  	commerce_shipworks_connector_generate_address("ShippingAddress", $shipping_address, $phone, $order_wrapper->mail->value());
	  }
	  catch (Exception $e) {
	  	watchdog('shipworks', 'No Shipping Information: ' .  $e->getMessage());
	  }
 
   	try {
   		//watchdog('shipworks', "Billing Address Properties<pre>" . print_r($billing->getPropertyInfo(), TRUE) . '</pre>');

    	$billing_address = $billing->commerce_customer_address;
    	$phone = $shipping->field_telephone_number->value();
    	commerce_shipworks_connector_generate_address("BillingAddress", $billing_address, $phone, $order_wrapper->mail->value());
	 }
    catch (Exception $e) {
    	watchdog('shipworks', 'No Billing Information: ' .  $e->getMessage());
    }
    _WriteOrderItems($order_wrapper);

    _WriteOrderTotals($order_wrapper);

    _writeStartTag("Debug");
      _writeElement("LastModifiedLocal", date("Y-m-d\TH:i:s", $order_wrapper->changed->value()));
    _writeCloseTag("Debug");
  _writeCloseTag("Order");
}

function commerce_shipworks_connector_generate_address($tag_name, $address, $phone = "", $email = "") {
	_writeStartTag($tag_name);
	  _writeElement("FullName", $address->name_line->value());
	  _writeElement("Company", $address->organisation_name->value());
	  _writeElement("Street1", $address->thoroughfare->value());
	  _writeElement("Street2", $address->premise->value());
	  _writeElement("Street3", "");
	  _writeElement("City", $address->locality->value());
	  _writeElement("State", $address->administrative_area->value());
	  _writeElement("PostalCode", $address->postal_code->value());
	  _writeElement("Country", $address->country->value());
	  _writeElement("Phone", $phone);
	  _writeElement("Email", $email);
	_writeCloseTag($tag_name);
}
// Write XML for all products for the given order
function _WriteOrderItems($order_wrapper) {

  _writeStartTag("Items");
    
  // Loop through product line items
  foreach ($order_wrapper->commerce_line_items as $index => $line_item_wrapper) {
    
    if ($line_item_wrapper->type->value() == 'product') {
   			//watchdog('shipworks', "Line Item Properties<pre>" . print_r($line_item_wrapper->getPropertyInfo(), TRUE) . "</pre>");

    	$product = $line_item_wrapper->commerce_product;
    	$product_id = $product->getIdentifier();
    	if ( empty($product_id) ) {
    		continue;
    	}

			// Unit price
      $unit_price = $line_item_wrapper->commerce_unit_price->amount->value();

      // Cost
      try {
      	$cost = $product->commerce_price->amount->value();
      } catch (Exception $e) {
				_writeStartTag("Debug");
				_writeElement("FAILED", 'Get Cost');
				_writeCloseTag("Debug"); 
				watchdog('shipworks', 'Bad product<pre>' . print_r($product, TRUE) . '</pre>');
			}     	



    // Weight
    //dpm($line_item_wrapper->getPropertyInfo()); 
    $product_fields = array_keys($product->getPropertyInfo());
		$product_name = $product->label();
		
    $variation = NULL;
    
    if ( $product_name == 'Antlerz' && in_array('field_dg_size_variation', $product_fields) ) {
    	$variation = $product->field_dg_size_variation->value();
    }
    
    	
    try {
    	$weight_amount = $product->field_shipping_weight->value();
    	$weight_amount = $weight_amount['weight'];
    } catch (Exception $e) {
    	$weight_amount = 0.00;
		}
		$name = $line_item_wrapper->commerce_product->title->value();
		
		if ( $weight_amount && empty($variation) ) {
		  $temp = (float)$weight_amount * 1000;
		  if ( $temp % 1000 == 0 ) {
		  	$places = 0;
		  }
		  else {
		  	$places = 2;
		  }
		  
			$w = number_format((float)$weight_amount, $places);
			
			$variation = format_plural($w, "@count lb", "@count lbs");
		}
		
		if ( !empty($variation) ) {
			$name .= ' ' . $variation;
		}
		
		
    _writeStartTag("Item");
      _writeElement("ProductID", $line_item_wrapper->commerce_product->product_id->value());
      _writeElement("SKU", $line_item_wrapper->commerce_product->sku->value());
      _writeElement("Code", $line_item_wrapper->commerce_product->sku->value());
      _writeElement("Name", $name);
      _writeElement("Quantity", round($line_item_wrapper->quantity->value()));
      _writeElement("UnitPrice", commerce_currency_amount_to_decimal($unit_price, STORE_CURRENCY));
      _writeElement("UnitCost", commerce_currency_amount_to_decimal($cost, STORE_CURRENCY));
      _writeElement("Weight", $weight_amount);
    _writeCloseTag("Item");
  }
}
  _writeCloseTag("Items");
}

// Write all totals lines for the order
function _WriteOrderTotals($order_wrapper) {

  $order_total = $order_wrapper->commerce_order_total->data->value();
	/*
  	    _writeStartTag("Debug");
    	// ->getPropertyInfo()
      _writeElement("Order Totals", "<pre>" . print_r($order_total, TRUE) . "</pre>");
    _writeCloseTag("Debug");
  */  
  _writeStartTag("Totals");

  foreach ($order_total['components'] as $delta => $data) {
    if (preg_match('/^commerce_coupon/', $data['name'])) {
      $value = commerce_currency_amount_to_decimal(abs($data['price']['amount']), STORE_CURRENCY);
      $attributes = array(
        "name" => 'Coupon',
        "class" => 'COUPON',
        "impact" => 'subtract',
      );
      _writeFullElement("Total", $value, $attributes);
    }
		if ( strpos($data['name'], 'tax|') === 0 ) {
      $value = commerce_currency_amount_to_decimal($data['price']['amount'], STORE_CURRENCY);
      $attributes = array(
        "name" => 'Sales Tax',
        "class" => 'TAX',
        "impact" => 'add',
      );
      _writeFullElement("Total", $value, $attributes);
    }
    if ($data['name'] == 'shipping') {
      $value = commerce_currency_amount_to_decimal($data['price']['amount'], STORE_CURRENCY);
      $attributes = array(
        "name" => 'Shipping',
        "class" => 'SHIPPING',
        "impact" => 'add',
      );
      _writeFullElement("Total", $value, $attributes);
    }
  }
  _writeCloseTag("Totals");
}

function _WriteNote($noteText, $dateAdded) {
  if (strlen($noteText) > 0) {
    $attributes = array("public" => "false",
      "date" => $dateAdded);

    _writeFullElement("Note", $noteText, $attributes);
  }
}

function _Action_UpdateStatus() {
  if (!isset($_REQUEST['order']) || !isset($_REQUEST['status'])) {
    _outputError(40, "Not all parameters supplied.");
    return;
  }

  if (is_numeric($_REQUEST['order'])) {
  	$codes = commerce_shipworks_connector_status_codes();
  	if ( !empty($_REQUEST['status']) && !empty($codes[$_REQUEST['status']]) ) {
    	$order_id = $_REQUEST['order'];
    	$status = $codes[$_REQUEST['status']];
    	watchdog('shipworks', ' in=' . $_REQUEST['status'] . ", out=$status");
    	
    	if ( $_REQUEST['status'] != 6 ) {
		    $order = commerce_order_load($order_id);
		  
		    // Update order status
		    commerce_order_status_update($order, $status, $skip_save = FALSE, $revision = TRUE, $log = 'Status updated through ShipWorks');
			}
			else {
				watchdog('shipworks', "Cancel order requested for order $order_id");
			}
			
	    echo "<UpdateSuccess/>";
    
  	}
		else {
        _outputError(40, "Not a valid order status.");
    }
  }
  else {
    _outputError(40, "Not a valid order id.");
  }
}

function _Action_UpdateShipment() {
  if (!isset($_REQUEST['order']) || !isset($_REQUEST['tracking'])) {
    _outputError(40, "Not all parameters supplied.");
    return;
  }

  if (is_numeric($_REQUEST['order'])) {
    $order_id = $_REQUEST['order'];
 
    $tracking = $_REQUEST['tracking'];
    $tracking_code = str_replace(' ', '', strtoupper($tracking));

		$carrier = NULL;
		if ( !empty($_REQUEST['Carrier']) ) {
			$carrier = strtoupper($_REQUEST['Carrier']);
		}
		
    if ( hare_shipping_record_tracking_code($order_id, $tracking_code, $carrier) ) {
      echo "<UpdateSuccess/>";
    }
  }
  else {
    _outputError(40, "Not a valid order id.");
  }
}

// Output xml declaration
function _writeXmlDeclaration() {
    echo "<?xml version=\"1.0\" standalone=\"yes\" ?>";
}

// Output start tag
function _writeStartTag($tag, $attributes = null) {
  echo '<' . $tag;

  if ($attributes != null) {
    echo ' ';

    foreach ($attributes as $name => $attribValue) {
      echo _toUtf8($name. '="'. htmlspecialchars($attribValue). '" ');
    }
  }

  echo '>';
}

// Output the given tag\value pair
function _writeElement($tag, $value) {
  _writeStartTag($tag);
  echo _toUtf8(htmlspecialchars($value));
  _writeCloseTag($tag);
}

// Outputs the given name/value pair as an xml tag with attributes
function _writeFullElement($tag, $value, $attributes) {
  echo _toUtf8('<'. $tag. ' ');

  foreach ($attributes as $name => $attribValue) {
    echo _toUtf8($name. '="'. htmlspecialchars($attribValue). '" ');
  }
  echo '>';
  echo _toUtf8(htmlspecialchars($value));
  _writeCloseTag($tag);
}

// Output close tag
function _writeCloseTag($tag) {
  echo _toUtf8('</' . $tag . '>');
}

// Convert to UTF-8
function _toUtf8($string) {
  return iconv("ISO-8859-1", "UTF-8//TRANSLIT", $string);
}

// Function used to output an error and quit.
function _outputError($code, $error) {
  _writeStartTag("Error");
  _writeElement("Code", $code);
  _writeElement("Description", $error);
  _writeCloseTag("Error");
}
 